package me.manenova.practica2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import java.math.BigDecimal
import java.math.RoundingMode

class MainActivity : AppCompatActivity() {

    private lateinit var txtAltura: TextView
    private lateinit var txtPeso: TextView
    private lateinit var btnCalcular: Button
    private lateinit var imgView: ImageView
    private lateinit var lblIMC: TextView
    private lateinit var lblDiagnostico: TextView

    var widthPerson: Double = 0.0
    var heightPerson: Double = 0.0
    var imcPerson: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        txtAltura = findViewById(R.id.txtAltura)
        txtPeso = findViewById(R.id.txtPeso)
        btnCalcular = findViewById(R.id.btnCalcular)
        lblIMC = findViewById(R.id.lblIMC)
        lblDiagnostico = findViewById(R.id.lblDiagnostico)
        imgView = findViewById(R.id.imgView)

        btnCalcular.setOnClickListener{
            widthPerson = txtAltura.text.toString().toDouble()
            heightPerson = txtPeso.text.toString().toDouble()
            imcPerson = "%.2f".format(widthPerson / (heightPerson * heightPerson)).toDouble()


            lblIMC.text = "IMC :"+imcPerson

            lblDiagnostico.text = when (true){
                (imcPerson <18.5) -> getString(R.string.text_imc1)
                (imcPerson in 18.50..24.99) -> getString(R.string.text_imc2)
                (imcPerson in 25.00..29.99) -> getString(R.string.text_imc3)
                (imcPerson >= 30) -> getString(R.string.text_imc4)
                else -> getString(R.string.text_imc4)
            }

            imgView.setImageResource(when (true){
                (imcPerson <18.5) -> R.drawable.underweight
                (imcPerson in 18.50..24.99) -> R.drawable.normalweight
                (imcPerson in 25.00..29.99) -> R.drawable.overweight
                (imcPerson >= 30) -> R.drawable.overweight
                else -> R.drawable.overweight
            })
        }
    }
}